SET GLOBAL local_infile=1;
GRANT ALL PRIVILEGES ON movielens.* TO 'root'@'%';
USE movielens;

CREATE TABLE Links (
    movieId VARCHAR(255) NULL,
    imdbId VARCHAR(255) NULL,
    tmdbId VARCHAR(255) NULL
);
LOAD DATA INFILE '/ml-latest-small/links.csv' INTO TABLE Links FIELDS TERMINATED BY ',' ENCLOSED BY '"' IGNORE 1
ROWS;


CREATE TABLE Movies (
    movieId VARCHAR(255) NULL,
    title VARCHAR(255) NULL,
    genres VARCHAR(255) NULL
);
LOAD DATA INFILE '/ml-latest-small/movies.csv' INTO TABLE Movies FIELDS TERMINATED BY ',' ENCLOSED BY '"' IGNORE 1
ROWS;
CREATE TABLE Ratings (
    userId VARCHAR(255) NULL,
    movieId VARCHAR(255) NULL,
    rating VARCHAR(255) NULL,
    timestamp VARCHAR(255) NULL
);
LOAD DATA INFILE '/ml-latest-small/ratings.csv' INTO TABLE Ratings FIELDS TERMINATED BY ',' ENCLOSED BY '"' IGNORE 1
ROWS;


CREATE TABLE Tags (
    userId VARCHAR(255) NULL,
    movieId VARCHAR(255) NULL,
    tag VARCHAR(255) NULL,
    timestamp VARCHAR(255) NULL
);
LOAD DATA INFILE '/ml-latest-small/tags.csv' INTO TABLE Tags FIELDS TERMINATED BY ',' ENCLOSED BY '"' IGNORE 1
ROWS;