import pandas as pd, time
from datetime import datetime
import mysql.connector

# MAIN
# While not connected to mySQL wait 15 seconds
connected = False
while not connected:
    try:
        time.sleep(15)
        connection = mysql.connector.connect(host='mysql',
                                        database='movielens',
                                        user='root',
                                        password='1234')
        connected = connection.is_connected()
    except:
        connected = False
cursor = connection.cursor()
print("Connected to MySQL Server Version ", connection.get_server_info())

# Get Movies table and convert it to pandas DF
cursor.execute("SELECT * FROM movielens.Movies")
data1 = cursor.fetchall()
moviesdf= pd.DataFrame(data1)
moviesdf.columns = ["movieId", "title", "genres"]
# Get Ratings table and convert it to pandas DF
cursor.execute("SELECT * FROM movielens.Ratings")
data2 = cursor.fetchall()
ratingsdf= pd.DataFrame(data2)
ratingsdf.columns = ["userId", "movieId", "rating", "timestamp"]
# Get Tags table and convert it to pandas DF
cursor.execute("SELECT * FROM movielens.Tags")
data3 = cursor.fetchall()
tagsdf= pd.DataFrame(data3)
tagsdf.columns = ["userId", "movieId", "tag", "timestamp"]

# QUESTIONS
print("--------------*********************************--------------")
print("1. How many movies are in data set ?")
print("There are: ", len(moviesdf), " movies.")
print("\n\n\n")


print("--------------*********************************--------------")
print("2. What is the most common genre of movie?")
moviesdf["splitted_genres"] = moviesdf["genres"].apply(lambda x: x.strip().split("|"))
mostCommonGenre = moviesdf.explode("splitted_genres").groupby("splitted_genres").count().sort_values(by="genres", ascending=False).index[0]
print("Most common genre is: ", mostCommonGenre, ".")
print("\n\n\n")


print("--------------*********************************--------------")
print("3. What are top 10 movies with highest rate ?")
ratingsdf["rating"] = ratingsdf["rating"].astype(float)
ratingsdfTranslated = pd.merge(ratingsdf, moviesdf, on=["movieId"], how="left")
top10ratedMoviesList = ratingsdfTranslated.groupby("title").agg({"rating": "mean"}).sort_values(by="rating", ascending=False).index[:10].tolist()
print("The top 10 moviest with highest rate are: ", top10ratedMoviesList)
print("\n\n\n")


print("--------------*********************************--------------")
print("4. What are 5 most often rating users ?")
mostOftenUsersIdList = tagsdf.groupby("userId").count().sort_values(by="timestamp", ascending=False).index[:5].tolist()
print("The 5 most often rating users: ", mostOftenUsersIdList)
print("\n\n\n")


print("--------------*********************************--------------")
print("5. When was done first and last rate included in data set and what was the rated movie tittle?")
ratingsdfTranslated["timestamp"] = ratingsdfTranslated["timestamp"].apply(lambda x: x.strip()).astype(int)
maxTimestamp = ratingsdfTranslated["timestamp"].max()
minTimestamp = ratingsdfTranslated["timestamp"].min()
maxTitle = ratingsdfTranslated[ratingsdfTranslated["timestamp"] == maxTimestamp].sort_values(by="rating", ascending=False).reset_index()["title"][0]
minTitle = ratingsdfTranslated[ratingsdfTranslated["timestamp"] == minTimestamp].sort_values(by="rating", ascending=False).reset_index()["title"][0]
print(minTitle, "is the movie with the oldest rate on", datetime.fromtimestamp(minTimestamp))
print(maxTitle, "is the movie with the newest rate on", datetime.fromtimestamp(maxTimestamp))
print("\n\n\n")


print("--------------*********************************--------------")
print("6. Find all movies released in 1990")
movies1990List = moviesdf[moviesdf["title"].str.contains("1990")]["title"].unique().tolist()
print("The list of movies released on 1990 are:", movies1990List)
print("\n\n\n")


