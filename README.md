# GROUPLENS APTITUDE TEST

Docker multicontainer application using one mySQL container and one python container.


### Requirements 📋

You will need to have docker configured in the machine you are running this code. To verify you have already installed docker:

```
docker -v
```

### How to run 🚀

The project uses a docker-compose.yml file in order to launch a multicontainer application. The application will launch first a mySQL container that will download a zipped folder that contains 3 datasets: Movies, Ratings and Tags. Then it will insert these datasets in mySQL tables in order to be ready for python container which will consult this tables. To launch the application type:

```
docker compose up
```

The application will run 2 differents Dockerfiles, one per container launched. The application will print the 6 questions with their 6 different answers.



### Author

- Manuel Aranda Villa
